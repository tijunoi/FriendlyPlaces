# FriendlyPlaces
[![Build Status](https://travis-ci.com/tijunoi/FriendlyPlaces.svg?branch=master)](https://travis-ci.com/tijunoi/FriendlyPlaces)

This is the final school project for DAM in La Salle Gràcia. It's an
Android app in Google Maps-style for reviewing and voting places where
it's safe for the LGBT+ community. You can alert of a bad treatment/
experience (always related only to LGBT+ issues) in any Google Maps place.

We use Firebase for backend.

Most of the code is written in Java, but we tried to code a little bit
in Kotlin.

This project uses Google Maps and Places API, and other third party libraries.

//Here there should be a list of credits for these libraries. Check app module gradle file in the meantime